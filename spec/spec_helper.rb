$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "telebots"

if ENV["CODACY_PROJECT_TOKEN"]
  require "codacy-coverage"
  Codacy::Reporter.start
else
  require "simplecov"
  SimpleCov.start
end

RSpec.configure do |config|
  config.before(:each) do
    Telebots::Bots.instance_variable_set("@collection", {})
  end
end
