require 'spec_helper'

RSpec.describe Telebots::Bot do
  let(:bot) do
    described_class.new('user_name_bot').tap do |bot|
      bot.token = 'token'
      bot.webhook = 'webhook'
      bot.commands.merge!(start: proc { [token, chat_id] })
    end
  end

  it { expect(bot.id).to eq 'user_name_bot' }
  it { expect(bot.token).to eq 'token' }
  it { expect(bot.webhook).to eq 'webhook' }
  it { expect(bot.commands.size).to eq 1 }
  it { expect(bot.can?(:start)).to eq true }
  it { expect(bot.execute(:start, 'chat_id')).to eq [bot.token, 'chat_id'] }
  it { expect(bot.can?(:help)).to eq false }
end
