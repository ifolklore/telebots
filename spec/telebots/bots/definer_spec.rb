require 'spec_helper'

RSpec.describe Telebots::Bots::Definer do
  let(:definer) do
    described_class.new('user_name_bot').tap do |definer|
      definer.token('token')
      definer.webhook('webhook')
      definer.commands(:start, :help, &proc { [token, chat_id] })
      definer.command(:test, &proc { [token, chat_id] })
    end
  end

  let(:bot) { definer.bot }

  it { expect(bot.id).to eq 'user_name_bot' }
  it { expect(bot.token).to eq 'token' }
  it { expect(bot.webhook).to eq 'webhook' }
  it { expect(bot.commands.size).to eq 3 }
  it { expect(bot.commands[:start]).to eq bot.commands[:help] }
  it { expect(bot.can?(:test)).to be true }
  it { expect(bot.execute(:help, 'chat_id')).to eq [bot.token, 'chat_id'] }
end
