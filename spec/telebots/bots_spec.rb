require 'spec_helper'

RSpec.describe Telebots::Bots do
  let(:bots) { described_class }

  before do
    bots.define 'first_bot' do
      token 'first_token'
      command(:start) { [token, chat_id] }
      command(:posts) { [token, chat_id] }
      webhook 'first_webhook'
    end

    bots.define 'second_bot' do
      token 'second_token'
      command(:start, :help) { [token, chat_id] }
      webhook 'second_webhook'
    end
  end

  let(:first_bot) { bots.all[0] }
  let(:second_bot) { bots.all[1] }

  describe '.define' do
    it { expect(first_bot.id).to eq 'first_bot' }
    it { expect(first_bot.token).to eq 'first_token' }
    it { expect(first_bot.webhook).to eq 'first_webhook' }
    it { expect(first_bot.commands.size).to eq 2 }

    it do
      expect(first_bot.execute(:start, 'chat_id')).
        to eq [first_bot.token, 'chat_id']
    end
    it do
      expect(first_bot.execute(:posts, 'chat_id')).
        to eq [first_bot.token, 'chat_id']
    end

    it do
      expect(second_bot.commands[:start]).to eq second_bot.commands[:help]
    end
    it do
      expect(second_bot.execute(:help, 'chat_id')).
        to eq [second_bot.token, 'chat_id']
    end
  end

  context 'methods of collection through method_missing' do
    describe '.each' do
      it do
        result = []
        bots.map { |bot| result << bot.id }
        expect(result).to eq ['first_bot', 'second_bot']
      end
    end

    describe '.map' do
      it { expect(bots.map { |bot| bot.id }).to eq ['first_bot', 'second_bot'] }
      it { expect(bots.map(&:id)).to eq ['first_bot', 'second_bot'] }
    end
  end

  describe '.find_by_webhook' do
    it { expect(bots.find_by_webhook('first_webhook')).to eq first_bot }
    it { expect(bots.find_by_webhook('second_webhook')).to eq second_bot }
  end

  describe '.push' do
    let(:third_bot) do
      Telebots::Bot.new('third_bot').tap do |bot|
        bot.token = 'third_token'
        bot.webhook = 'third_webhook'
        bot.commands.merge!(start: proc { [token, chat_id] })
      end
    end

    before { bots.push(third_bot) }

    it { expect(bots.all.size).to eq 3 }
    it { expect(bots.all[2]).to eq third_bot }
  end

  describe '.all' do
    it { expect(bots.all.size).to eq 2 }
  end
end
