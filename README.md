# Telebots [![Build Status](https://drone.io/bitbucket.org/ifolklore/telebots/status.png)](https://drone.io/bitbucket.org/ifolklore/telebots/latest) [![Codacy Badge](https://api.codacy.com/project/badge/Grade/d7a4f7837a6f44b98abc0a35c6fe3557)](https://www.codacy.com/app/stanislav-gordanov/telebots)

DSL for defining Telegram bots.

## Usage

#### Define bot example:
```ruby
Telebots::Bots.define 'AnyBotName' do
  token 'secret'.freeze

  command :any_command do
    text_message = FakeMessage.generate

    FakeTelegramMessage.new(token, chat_id).send(text_message)
  end

  commands :any_command_2, :any_command_3 do
    text_messages = FakeMessages.generate
    fake_telegram_message = FakeTelegramMessage.new(token, chat_id)

    text_messages.each do |text_message|
      fake_telegram_message.send(text_message)
    end
  end

  webhook 'slug'.freeze
end
```

#### Description of *Telebots::Bots::Definer* methods:
- *token* - sets the bot token;
- *command* - and its alias *commands* define the bot commands. Chat_id and Token variables available in block for sending a response to questioner;
- *webhook* - specify a url/path for receive incoming updates.

#### Description of *Telebots::Bots* methods:
- *define* - creates a new bot and adds it to list;
- *find_by_webhook* - finds the first bot which matching the webhook in list, O(1);
- *all* - returns list of bots;
- *each* - calls the given block once for each bot in list, passing bot as a parameter.

#### Execute command example:
```ruby
bot = Telebots::Bots.find_by_webhook('slug')
return if bot.nil?

payload = FakeUpdater.parse(updates)
return unless bot.can?(payload[:command])

bot.execute(payload[:command], payload[:chat_id])
```
#### Description of *Telebots::Bot* methods:
- *can?* - checks to exist bot command;
- *execute* - runs bot command for chat_id.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'telebots'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install telebots

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
