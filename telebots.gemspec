# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'telebots/version'

Gem::Specification.new do |s|
  s.name          = "telebots"
  s.version       = Telebots::VERSION
  s.authors       = ["Stanislav Gordanov"]
  s.email         = ["stanislav.gordanov@gmail.com"]

  s.summary       = %q{DSL for defining Telegram bots.}
  s.description   = %q{DSL for defining Telegram bots.}
  s.homepage      = "https://bitbucket.org/ifolklore/telebots"
  s.license       = "MIT"

  s.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  s.bindir        = "exe"
  s.executables   = s.files.grep(%r{^exe/}) { |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_development_dependency "bundler", ">= 1"
  s.add_development_dependency "rake", ">= 11"
  s.add_development_dependency "rspec", ">= 3"
  s.add_development_dependency "simplecov"
  s.add_development_dependency "codacy-coverage"
end
