require "telebots/bots/definer"

module Telebots
  class Bots
    @collection ||= {}

    # Public.
    #
    # id - String.
    #
    # Returns nothing.
    def self.define(id, &block)
      definition = Definer.new(id)
      definition.instance_eval(&block)

      push(definition.bot)
    end

    # Public.
    #
    # path - String.
    #
    # Returns Telebots::Bots::Bot.
    def self.find_by_webhook(path)
      @collection[path]
    end

    # Public.
    #
    # Returns Array<Telebots::Bots::Bot>.
    def self.push(bot)
      @collection.merge!(bot.webhook => bot)

      all
    end

    # Public.
    #
    # Returns Array<Telebots::Bots::Bot>.
    def self.all
      @collection.values
    end

    def self.method_missing(name, *args, &block)
      if %i{each map}.include? name
        if block
          all.send(name, &block)
        else
          all.send(name, *args)
        end
      else
        super
      end
    end
  end
end
