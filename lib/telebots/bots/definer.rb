module Telebots
  class Bots
    class Definer
      attr_reader :bot

      # Public: Initialize a Telebots::Bots::Definer.
      #
      # id - String, telegram like "username".
      #
      # Returns instance of Telebots::Bots::Definer.
      def initialize(id)
        @bot = ::Telebots::Bot.new(id)
      end

      # Public: Sets bot token.
      #
      # string - String, telegram bot token.
      #
      # Returns nothing.
      def token(string)
        bot.token = string
      end

      # Public.
      #
      # names - Symbol or Array<Symbol>;
      # block - block.
      #
      # Returns nothing.
      def command(*names, &block)
        names.each { |name| bot.commands.merge!(name => block) }
      end
      alias commands command

      # Public.
      #
      # path - String.
      #
      # Returns nothing.
      def webhook(path)
        bot.webhook = path
      end
    end
  end
end
