module Telebots
  class Bot
    attr_accessor :id, :commands, :token, :webhook

    # Public: Initialize a Telebots::Bots::Bot.
    #
    # id - String.
    #
    # Returns instance of Telebots::Bots::Bot.
    def initialize(id)
      @id = id
      @commands = {}
    end

    # Public.
    #
    # name - Symbol.
    #
    # Returns Boolean.
    def can?(name)
      commands.keys.include? name
    end

    # Public.
    #
    # name    - Symbol;
    # chat_id - String or Integer.
    #
    # Returns nothing.
    def execute(name, chat_id)
      Struct.new(:token, :chat_id)
            .new(token, chat_id)
            .instance_exec(&commands[name])
    end
  end
end
